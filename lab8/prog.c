#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;
typedef long long ll;

ll rdtsc() {
        volatile unsigned int edx, eax;
        asm("rdtsc":"=d"(edx),"=a"(eax));
        return ((((ll)edx)<<32) | ((ll) eax));
}
int buf[8*1024*1024+5];
int buf2[8*1024*1024+5];
int main(int argc, char** argv) {
        cout << setprecision(10);
        for (int arr_size = 256;arr_size<=8*1024*1024;arr_size+=arr_size/10) {
                cout << arr_size*4 << " ; ";
                ll counter1, counter2;
                double clocks_per_access;
                for (int i=0;i<arr_size;i++) {
                        buf[i] = i+1;
                }
                buf[arr_size-1] = -1;
                int it = 0;
                while(buf[it]!=-1) {
                        it = buf[it];
                };
                it = 0;
                counter1 = rdtsc();
                while (buf[it]!=-1) {
                        it = buf[it];
                }
                counter2 = rdtsc();
                clocks_per_access = ((double)(counter2-counter1))/((double) arr_size);
                cout << clocks_per_access << " ; ";
                for (int i=0;i<arr_size;i++) {
                        buf[i] = i-1;
                }
                buf[0] = -1;
                it = arr_size-1;
                counter1 = rdtsc();
                while (buf[it]!=-1) {
                        it = buf[it];
                }
                counter2 = rdtsc();
                clocks_per_access = ((double)(counter2-counter1))/((double) arr_size);
                cout << clocks_per_access << " ; ";
                                for (int i=0;i<arr_size;i++) {
                        buf[i] = i-1;
                }
                                for (int i=0;i<arr_size;i++) {
                                        buf2[i] = i;
                                }
                                for (int i=arr_size-1;i>=1;i--) {
                                        int j = rand() % i;
                                        swap(buf2[i], buf2[j]);
                                }
                                for (int i=0;i<arr_size-1;i++) {
                                        buf[buf2[i]] = buf2[i+1];
                                }
                                buf[buf2[arr_size-1]] = -1;
                                it = buf2[0];
                counter1 = rdtsc();
                while (buf[it]!=-1) {
                        it = buf[it];
                }
                counter2 = rdtsc();

                clocks_per_access = ((double)(counter2-counter1))/((double) arr_size);
                cout << clocks_per_access;
                cout << endl;
        }
        return 0;
}
