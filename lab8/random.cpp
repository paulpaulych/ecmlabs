#include <iostream>
#include <ctime>

void fillRandom(int* a, int size) {
	for (int i = 0; i < size; ++i) {
		a[i] = i;
	}
	std::srand(unsigned(std::time(0)));
	int random;
	for (int i = size - 1; i > 0; --i) {
		for(int j = 0; j < size; j++){
			std::cout << a[j] << " ";
		}
		random = std::rand() % i;
		std::swap(a[i], a[random]);
		std::cout << std::endl;
	}
}

int main(){
	int size = 10;
	int * a = new int[size];
	fillRandom(a, size);
		
}
