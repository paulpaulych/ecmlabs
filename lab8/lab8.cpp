#include <cstdlib>
#include <iostream>
#include <ctime>
#include <climits>
#include <vector>	
#include <random>       
#include <fstream>   

static const int K = 1;

inline unsigned long long rdtsc() {
	unsigned long long  lo, hi;
	asm volatile("rdtsc\n" : "=a" (lo), "=d" (hi));
	return (((unsigned long long)hi << 32) | (unsigned long long)lo);
}

void fillDirect(int* a, int size) {
	for (int i = 0; i < size - 1; ++i) {
		a[i] = i + 1;
	}
	a[size-1] = 0;
}


void fillReverse(int* a, int size) {
	a[0] = size - 1;	
	for (int i = 1; i < size; ++i) {
		a[i] = i - 1;
	}
}

void fillRandom(int* a, int size) {
	for (int i = 0; i < size; ++i) {
		a[i] = i;
	}

	std::srand(unsigned(std::time(0)));
	int random;
	for (int i = size - 1; i > 0; --i) {
		random = std::rand() % i;
		std::swap(a[i], a[random]);
	}
}

void arrayTraversing(int* a, int size) {
	int i, k;
	for (k = 0, i = 0; i < size * K; ++i) {
		k = a[k];
	}
}

double direct_test(int size) {
	uint64_t start, end;

	int* direct_array = new int[size]();
	fillDirect(direct_array, size);

	arrayTraversing(direct_array, size);

	unsigned long min_time = ULONG_MAX;
	
	for(int i = 0; i < 10; i++){
		start = rdtsc();
		arrayTraversing(direct_array, size);
		end = rdtsc();
		if(end-start < min_time) min_time = end-start;
	}
	
	delete[] direct_array;
	return (double)min_time / (size * K);
}

double reverse_test(int array_size) {
	uint64_t start, end;

	int* reverse_array = new int[array_size]();
	fillReverse(reverse_array, array_size);
	
	arrayTraversing(reverse_array, array_size);

	unsigned long min_time = ULONG_MAX;
	for(int i = 0; i < 10; i++){
		start = rdtsc();
		arrayTraversing(reverse_array, array_size);
		end = rdtsc();
		if(end-start < min_time) min_time = end-start;
	}
	
	delete[] reverse_array;
	return (double)min_time / (array_size * K);
}

double random_test(int array_size) {
	uint64_t start, end;
	
	int* random_array = new int[array_size]();
	fillRandom(random_array, array_size);
	
	arrayTraversing(random_array, array_size);

	unsigned long min_time = ULONG_MAX;
	for(int i = 0; i < 10; i++){
		start = rdtsc();
		arrayTraversing(random_array, array_size);
		end = rdtsc();
		if(end-start < min_time) min_time = end-start;
	}
	
	delete[] random_array;
	return (double)min_time / (array_size * K);
}

int main() {
	const int min_size = 256;
	const int max_size = 8388608;
	std::ofstream fout("fout.csv");	
	int N = min_size;
	while (N <= max_size) {
		std::cout << "Array size: " << N * 4 / 1024 << std::endl;
		std::cout << "Direct: " << direct_test(N) << std::endl;
		std::cout << "Reverse: " << reverse_test(N) << std::endl;
		std::cout << "Random: " << random_test(N) << std::endl;
		std::cout << std::endl;
		
		fout << N * 4/1024 << ", " << direct_test(N) <<
		", " << reverse_test(N) <<
		", " << random_test(N) << std::endl;

		N += N/30+3;
	}
	fout.close();
	return 0;
}
