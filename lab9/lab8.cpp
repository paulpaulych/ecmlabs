#include <cstdlib>
#include <iostream>
#include <ctime>
#include <climits>
#include <vector>	
#include <random>       
#include <fstream>   

static const int K = 3;
static const int offset = 2097152;//2097152; // 256 * 32 KB (32 * L2cache_)	
static const int cacheSize = 65536; // 256 * 32 KB (32 * L2cache_)	

inline unsigned long long rdtsc() {
	unsigned long long  lo, hi;
	asm volatile("rdtsc\n" : "=a" (lo), "=d" (hi));
	return (((unsigned long long)hi << 32) | (unsigned long long)lo);
}

void fill(int * a, int blockNum) {
	for (size_t j = 0; j < cacheSize / blockNum; j++) {
		for (size_t i = 0; i < blockNum; i++) {
			a[i*offset + j] = ((i + 1)*offset + j);
		}
	a[(blockNum - 1) * offset + j] = (j + 1) % 1;
	}
}

void arrayTraversing(int* a, size_t blockNum) {
	int i, k;
	for (k = 0, i = 0; i < offset * blockNum; ++i) {
		k = a[k];
	}
}

double test(int blockNum) {
	uint64_t start, end;
	int *array = new int[offset * blockNum];
	fill(array, blockNum);
	
	arrayTraversing(array, blockNum);

	unsigned long min_time = ULONG_MAX;
	for(int i = 0; i < K; i++){
		start = rdtsc();
		arrayTraversing(array, blockNum);
		end = rdtsc();
		if(end-start < min_time) min_time = end-start;
	}
	
	delete[] array;
	return (double)min_time / (offset * blockNum);
}

int main() {
	std::ofstream fout("fout.csv");	
	for (int blockNum = 1; blockNum <= 32; ++blockNum) {
		std::cout << blockNum << ", " << test(blockNum) << std::endl;		
		fout << blockNum << ", " << test(blockNum) << std::endl;
	}
	fout.close();
	return 0;
}
