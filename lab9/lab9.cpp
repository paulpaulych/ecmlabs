#include <iostream>
#include <fstream>

const int offset = 2097152; // 8 Mb
const int cache_size = 65536; // 256 Kb
const int K = 4;
static const int MAX_N = 32;

int* initArray(int fragmentsCount, int fragment_size){
	int* array = new int[offset * fragmentsCount];
	for (size_t j = 0; j < fragment_size; j++)
	{
		for (size_t i = 0; i < fragmentsCount; i++)
		{
		array[i*offset + j] = ((i + 1)*offset + j);
		}
	array[(fragmentsCount - 1)*offset + j] = (j + 1) % fragment_size;
	}
	return array;
}

unsigned __int64 arrayTraversing(int* array, int fragmentsCount, int fragment_size){
	unsigned __int64 start, end, tmpMin, currMin = (0 - 1);
	int k = 0;
	int size = fragmentsCount * fragment_size;

	for (int c = 0; c < K; c++){
		start = __rdtsc();
		for (size_t i = 0; i < size; i++) {
			k = array[k];
		}
		end = __rdtsc();
		tmpMin = end - start;
		if (tmpMin < currMin){
			currMin = tmpMin;
		}
	}
	return currMin / (fragmentsCount * fragment_size);
}

int main(int argc, char** argv)
{
	int* array;
	int size;
	unsigned __int64 result;

	std::ofstream outfile(argv[1]);
	for (int N = 1; N < MAX_N + 1; ++N)
	{
		size = cache_size / N;
		array = initArray(N, size);
		result = arrayTraversing(array, N, size);
		outfile « N « ';' « result « '\n';
		delete[] array;
	}
}
