#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

void identity(float *I, int N) {
	for (int i = 0; i < N; i++)
	I[i * (N + 1)] = 1;
}

void Add(float *Res, float *A, float *B, int N) {
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			int index = i * N + j;
			Res[index] = A[index] + B[index];	
		}
	}
}

void Sub(float *Res, float *A, float *B, int N) {
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++) {
			int index = i * N + j;
			Res[index] = A[index] - B[index];
		}
	}
}

void transpose(float *Res, float *A, int N) {
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			Res[i * N + j] = A[j * N + i];
		}
	}
}

float MaxColSum(float *A, int N) {
	float max = 0;
	for (int j = 0; j < N; j++)
	{
		float sum = 0;
		for (int i = 0; i < N; i++)
		sum += fabs(A[i * N + j]);
		if (sum > max)
		max = sum;
	}
	return max;
}

float MaxRowSum(float *A, int N)
{
	float max = 0;
	for (int i = 0; i < N; i++) {
	float sum = 0;
	for (int j = 0; j < N; j++)
		sum += fabs(A[i * N + j]);
		if (sum > max)
			max = sum;
	}

	return max;

}

void Mul(float *Res, float *A, float *B, int N) {
	float *transB = (float*)malloc(sizeof(float) * N * N);
	transpose(transB, B, N);
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			float sum = 0;
			for (int r = 0; r < N; r++) {
				sum += A[i * N + r] * transB[j * N + r];
				Res[i * N + j] = sum;
			}
		}
	}
}


void MulNumber(float *Res, float *A, float number, int N) {
	for (int i = 0; i < N * N; i++)
	Res[i] = A[i] * number;
}

void swap(float **ptr1, float **ptr2){
	float *tmp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = tmp;
}

int main() {
	srand(time(NULL));
	int N = 1000;
	int M = 10;
	float *I = (float*)calloc(sizeof(float), N * N);
	float *A = (float*)calloc(sizeof(float), N * N);
	float *temp = (float*)calloc(sizeof(float), N * N);
	float *temp2 = (float*)calloc(sizeof(float), N * N);
	float *B = (float*)calloc(sizeof(float), N * N);
	float *R = (float*)calloc(sizeof(float), N * N);
	float *transA = (float*)calloc(sizeof(float), N * N);
	float *revA = (float*)calloc(sizeof(float), N * N);
	for (int i = 0; i < N * N; i++){
		A[i] = rand() % 1000;
	}
	clock_t start = clock();
	identity(I, N);
	float denominator = MaxColSum(A, N) * MaxRowSum(A, N);
	transpose(transA, A, N);
	MulNumber(B, transA, 1 / denominator, N);
	Mul(temp, B, A, N);
	Sub(R, I, temp, N);
	memcpy(temp, R, N * N * sizeof(float));
	for (int i = 0; i < M; i++){
		Add(I, I, temp, N);
		Mul(temp2, temp, R, N);
		swap(&temp, &temp2);
	}
	Mul(revA, I, B, N);
	clock_t end = clock();
	printf("time = %.10lf sec\n", (double)(end - start) / CLOCKS_PER_SEC);
	return 0;
}
