#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <xmmintrin.h>
#include <emmintrin.h>

void identity(float *I, int N){
	memset(I, 0, N*N * sizeof(float));
	for (int i = 0; i < N; i++)
	I[i * (N + 1)] = 1;
}

void Add(float *Res, float *A, float *B, int N)
{
	__m128 *mRes = (__m128*)Res;
	__m128 *mA = (__m128*)A;
	__m128 *mB = (__m128*)B;
	for (int i = 0; i < N * N / 4; i++){
		mRes[i] = _mm_add_ps(mA[i], mB[i]);
	}
}
void Sub(float *Res, float *A, float *B, int N) {
	__m128 *mRes = (__m128*)Res;
	__m128 *mA = (__m128*)A;
	__m128 *mB = (__m128*)B;
	size_t i = 0;
	for (int i = 0; i < N * N / 4; i++){
		mRes[i] = _mm_sub_ps(mA[i], mB[i]);
	}
}
void transpose(float *Res, float *A, int N) {
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			Res[i * N + j] = A[j * N + i];
		}
	}
}
float MaxRowSum(float *A, int N) {
	float max = 0.0f;
	__m128 *mA = (__m128*)A;
	__m128 *block = mA;
	__m128 mMask = _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF)); //Маска для abs
	for (int i = 0; i < N; i++) {
		float sum = 0.0f;
		__m128 mSum = _mm_setzero_ps(); //Зануление вектора
		__m128 temp;
		for (int j = 0; j < N / 4; j++) {
			temp = _mm_load_ps((float*)block); //Загружаем в вектор temp 4 числа по указателю block
			temp = _mm_and_ps(temp, mMask); //Считаем модули компонент вектора temp
			mSum = _mm_add_ps(mSum, temp); //Суммируем
		}
		float four[4];
		_mm_store_ps(four, mSum); //Получаем массив компонент
		sum = four[0] + four[1] + four[2] + four[3];
		if (max < sum){
			max = sum;
		}
	}
	return max;
}
void Mul(float *Res, float *A, float *B, int N) {
	float *transB = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	transpose(transB, B, N);
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++) {
			__m128 mSum = _mm_setzero_ps();
			for (int r = 0; r < N / 4; r++) {
				__m128 temp1 = _mm_load_ps(A + i * N + r * 4);
				__m128 temp2 = _mm_load_ps(transB + j * N + r * 4); 
				mSum = _mm_add_ps(mSum, _mm_mul_ps(temp1, temp2)); 
			}			}
			float sum = 0.0f;
			__m128 buf = mSum;
			buf = _mm_movehl_ps(buf, mSum);
			mSum = _mm_add_ps(mSum, buf);
			buf = _mm_shuffle_ps(mSum, mSum, 1);
			mSum = _mm_add_ss(mSum, buf);
			_mm_store_ss(&sum, mSum);
			Res[i * N + j] = sum;
		}
	}
}

void MulNumber(float *Res, float *A, float number, int N) {
	__m128 *mRes = (__m128*)Res;
	__m128 *mA = (__m128*)A;
	__m128 mNumber = _mm_load1_ps(&number);
	for (int i = 0; i < N * N / 4; i++){
		mRes[i] = _mm_mul_ps(mA[i], mNumber);
	}
}

void swap(float **ptr1, float **ptr2) {
	float *tmp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = tmp;
}

int main() {
	srand(time(NULL));
	int N = 12;
	int M = 10;
	int i = 0;
	float *I = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *A = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *temp = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *temp2 = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *B = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *R = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *transA = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	float *revA = _mm_malloc(N * N * sizeof(float), 4 * sizeof(float));
	for (int i = 0; i < N * N; i++)
	A[i] = rand() % 1000;
	clock_t start = clock();
	identity(I, N);
	transpose(transA, A, N);
	float denominator = 1.0f / (MaxRowSum(A, N) * MaxRowSum(transA, N));
	MulNumber(B, transA, denominator, N);
	Mul(temp, B, A, N);
	Sub(R, I, temp, N);
	memcpy(temp, R, N * N * sizeof(float));
	while (i < M) {
		Add(I, I, temp, N);
		Mul(temp2, temp, temp, N);
		swap(&temp, &temp2);
		i++;
	}
	Mul(revA, I, B, N);
	clock_t end = clock();
	printf("time = %.10lf sec\n", (double)(end - start) / CLOCKS_PER_SEC);
}
