#include <string.h>
#include <stdio.h>
#include <cblas.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>

void identity(float *I, int N) {
	for (int i = 0; i < N; i++){
		I[i * (N + 1)] = 1;
	}
}
float MaxRowSum(float *A, const int N){
	float *sum_arr = (float*)calloc(sizeof(float*), N);
	for (int i = 0; i < N; i++){
		sum_arr[i] = cblas_sasum(N, A + N * i, 1);
	}
	float max_sum = sum_arr[cblas_isamax(N, sum_arr, 1)];
	free(sum_arr);
	return max_sum;
}
float MaxColSum(float *A, int N){
	float *sum_arr = (float*)calloc(sizeof(float*), N);
	for (int i = 0; i < N; i++) {
		sum_arr[i] = cblas_sasum(N, A + i, N);	
	}
	float max_sum = sum_arr[cblas_isamax(N, sum_arr, 1)];
	free(sum_arr);
	return max_sum;
}
int main() {
	srand(time(NULL));
	int N = 2048;
	int M = 10;
	float *I = (float*)calloc(sizeof(float), N * N);
	float *A = (float*)calloc(sizeof(float), N * N);
	float *temp = (float*)calloc(sizeof(float), N * N);
	float *temp2 = (float*)calloc(sizeof(float), N * N);
	float *R = (float*)calloc(sizeof(float), N * N);
	float *revA = (float*)calloc(sizeof(float), N * N);
	for (int i = 0; i < N * N; i++)
	A[i] = rand() % 1000;
	clock_t start = clock();
	identity(revA, N);
	float denominator = 1.0f / (MaxRowSum(A, N) * MaxColSum(A, N));
	cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans, N, N, N, denominator, A, N, A, N, 0, R, N);
	cblas_saxpy(N * N, - 1.0f, R, 1, revA, 1);
	memcpy(temp, revA, N * N * sizeof(float));
	memcpy(R, revA, N * N * sizeof(float));
	identity(I, N);
	for (int i = 0; i < M; i++) {
		cblas_saxpy(N *N, 1.0f, temp, 1, I, 1);
		cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0f, temp, N, R, N, 0, temp2, N);
		memcpy(temp, temp2, N * N * sizeof(float));
	}
	cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans, N, N, N, denominator, I, N, A, N, 0, revA, N);
	clock_t end = clock();
	printf("time = %.10lf sec\n", (double)(end - start) / CLOCKS_PER_SEC);
	return 0;
}
